// 소켓통신을 위해 네트워크 모듈을 불러온다.
let net = require('net');

// 파일시스템을 사용하기 위한 모듈을 불러온다.
let fs = require('fs');

// HTTP Response 에서 보내줄 Content-type 헤더를 만들어줄 모듈 불러온다.
let mimeTypes = require('mime-types');

// 웹브라우저로 제공할 리소스들이 모여있는 폴더
let documentRoot = './www';
// 리소스 경로를 생략시 뒤에 붙여줄 기본파일명 할당
let index = 'index.html';

// 서버소켓을 생성한다.
let serverSocket = net.createServer(clientSocket => {
    // 클라이언트가 접속되어 데이터를 보내왔을 때
    clientSocket.on('data', buf => {
        /* 보내온 데이터를 화면에 출력한다.
        console.log(buf.toString());*/
        /* 보네온 데이터를 HTTP 규격에 따라 분해한다.
        네크워크 통신은 byte 단위 통신으로 Buffer 객체인 데이터로 각 행마다
        줄바꿈 기호로 구분할 수 있다.*/
        let rawData = buf.toString();
        // 매 행을 줄바꿈 기호로 split 한다.
        let rawDataSplit = rawData.split("\r\n");
        // 첫 행 HTTP 메서드 와 리소스경로, HTTP 버전을 " " 로 구분하여 3개의 배열을 할당
        let firstRowArray = rawDataSplit[0].split(" ");
        // 첫 행 배열을 담을 변수선언 및 할당(첫 행 분석)
        let HTTPRequestMethod, HTTPRequestResource, HTTPVersion, HTTPHeaders = {};
        HTTPRequestMethod = firstRowArray[0];
        HTTPRequestResource = firstRowArray[1];
        // 리소스 경로생략시, 즉 폴더경로로 끝날 경우, 기본파일명을 붙여준다.
        if (HTTPRequestResource[HTTPRequestResource.length - 1] == '/') {
            HTTPRequestResource += index;
        }
        HTTPVersion = firstRowArray[2];
        // HTTP Request Header 분석 (key: value 형태)
        for (let line = 1; line < rawDataSplit.length; line++) {
            // 요청 받아온 rawDataSplit 마지막줄은 "\r\n" 줄바꿈만 있고 빈행으로 break 한다.
            if (!rawDataSplit[line]) {
                break;
            }
            // Header 를 key 와 value 형태로 분류한다.
            let HeaderKey = rawDataSplit[line].split(': ')[0];
            // Header key의 첫글자를 소문자로 변경한다.
            HeaderKey = HeaderKey.replace(HeaderKey[0], HeaderKey[0].toLowerCase());
            // key 에 "-"가 여러개이므로 삭제한다.
            while (HeaderKey.indexOf('-') >= 0) {
                HeaderKey = HeaderKey.replace('-', '');
            }
            let HeaderValue = rawDataSplit[line].split(': ')[1];
            HTTPHeaders[HeaderKey] = HeaderValue;
        }
        console.log('웹브라우저가 ' + HTTPRequestMethod + '메서드로 ' + HTTPRequestResource + '리소스를 요청하였습니다.');
        console.log(HTTPHeaders);

        // 리소스의 존재여부를 확인을 하고 그 결과를 할당한다.
        let isExists = fs.existsSync(documentRoot+HTTPRequestResource);
        // 이 서버에 존재하지 않는 리소스에 대한 요청을 받은 경우
        if (!isExists) {
            // HTTP Response content 문서 할당
            let content = '<html><head><title>File Not Found</title><meta charset="utf-8"></head><body>파일이 존재하지 않습니다.</body></html>';
            // http 버전  응답코드  응답메시지 및 필수 헤더
            let response = 'HTTP/1.1 404 File Not Found\r\n';
            response += 'Content-Type: text/html\r\n';
            response += 'Content-Length: '+ (Buffer.from(content).length) +'\r\n';
            response += '\r\n';
            clientSocket.write(Buffer.from(response));
            clientSocket.write(Buffer.from(content));
        } else {
            // 이 서버에 존재하는 리소스에 대한 요청을 받은 경우
            // root 폴더에 있는 리소스를 읽어 온다.
            let content = fs.readFileSync(documentRoot+HTTPRequestResource);
            // 해당 리소스를 응답하므로 응답코드는 200으로 한다.
            let response = 'HTTP/1.1 200 OK\r\n';
            response += 'Content-Type: '+ mimeTypes.lookup(documentRoot+HTTPRequestResource) +'\r\n';
            response += 'Content-Length: '+ (content.length) +'\r\n';
            response += '\r\n';
            clientSocket.write(Buffer.from(response));
            clientSocket.write(Buffer.from(content));
        }
    })
});

// 소버소켓을 개방한다.(원래 http 프로토콜을 사용하면 80포트이다)
serverSocket.listen(3000, () => {
    console.log('HTTP 서버가 3000번 포트에 준비되었습니다.')
});